<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idgrado')->unsigned();
            $table->integer('idasignatura')->unsigned();
            $table->boolean('activo')->default(true);
            $table->foreign('idgrado')->references('id')->on('grados')->onDelete('cascade');
            $table->foreign('idasignatura')->references('id')->on('asignaturas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributions');
    }
}
