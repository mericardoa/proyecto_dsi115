<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('notas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('idrecord')->unsigned();
          $table->integer('idtrimestre')->unsigned();
          $table->float('nota1');
          $table->float('nota2');
          $table->float('nota3');
          $table->float('promedio');
          $table->integer('idasignatura')->unsigned();
          $table->foreign('idrecord')->references('id')->on('records')->onDelete('cascade');
          $table->foreign('idasignatura')->references('id')->on('asignaturas')->onDelete('cascade');
          $table->foreign('idtrimestre')->references('id')->on('trimestres')->onDelete('cascade');
          

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('notas');
        //
    }
}
