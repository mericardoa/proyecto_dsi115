<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idalumno')->unsigned();
            $table->integer('idgrado')->unsigned();
            $table->boolean('activo')->default(true);
            $table->foreign('idalumno')->references('id')->on('alumnos')->onDelete('cascade');
            $table->foreign('idgrado')->references('id')->on('grados')->onDelete('cascade');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
