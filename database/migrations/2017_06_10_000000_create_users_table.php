<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iddocente')->unsigned();
            $table->string('email', 60)->unique();
            $table->enum('rol', ['Director', 'Docente', 'Docente Parvularia'])->default('Docente');
            $table->string('password');
            $table->foreign('iddocente')->references('id')->on('docentes')->onDelete('cascade');
            $table->boolean('activo')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
