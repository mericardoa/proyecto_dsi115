@extends('templates.main')

@section('title', 'Inicio')


@section('content')

<div class="form-group">

  <FONT FACE="impact" SIZE=5 COLOR="#0B0B61"> <h1 align=center>COMPLEJO EDUCATIVO CANTÓN LAS ISLETAS</h1> </font>

<FONT FACE="impact" SIZE=5 COLOR="#0B0B61">
<h2>VISIÓN</h2>
</FONT>
<FONT FACE="roman" SIZE=5 COLOR="#0B0B61">
 <p class="text-justify">
Ser una institución inclusiva de tiempo pleno , comprometida a desarrollar competencias y habilidades con nuevas propuestas curriculares y la implementación de metodologías activas, basadas en la práctica de valores éticos, morales y cívicos, que permitan el desarrollo integral del estudiantado, transformándolo en un ente de cambio social.</p>
</FONT>

<FONT FACE="impact" SIZE=5 COLOR="#0B0B61">
<h2>MISIÓN</h2>
</FONT>

<FONT FACE="roman" SIZE=5 COLOR="#0B0B61">
<p class="text-justify">
Somos una institución inclusiva de tiempo pleno, dedicada a brindar  una educación de calidad humana, fomentando la identidad cultural,   basados en el respeto  y la disciplina, mediante la aplicación de metodologías activas, que involucra a toda la comunidad educativa y le permite construir aprendizajes significativos al estudiantado.</p>
</FONT>

    </div>




@endsection
