@extends('templates.main')

@section('title', 'Historial de Notas del Alumno')

@section('content')

    <h3 align="center">{{$identificador->nombre}}</h3>
    <h3 align="center">{{$alumno->nombres}} {{$alumno->apellido_padre}} {{$alumno->apellido_madre}}</h3>
    <h3 align="center">Notas Finales</h3>


    {!! Form::open(['route'=>['notas.update', $identificador], 'method'=>'PUT']) !!}

<!--Registro de Notas Finales -->
</hr>
<div class="container">
  <div style="float:left;width:50%">
<table class="table table-condensed">
    <thead>
      <th>Materias</th>
      <th>Primer Trimestre</th>
      <th>Segundo Trimestre</th>
      <th>Tercer Trimestre</th>
    </thead>
    <tbody>
        @foreach($atribuciones as $atribucion)
           @if($atribucion->idgrado == $identificador->id)
            <tr>
              <td>{{ $atribucion->asignaturas->nombre }}</td>
                   @foreach($notas as $nota)
                       @if($nota->records->idgrado == $identificador->id)
                           @if($nota->records->idalumno == $alumno->id)
                              @if($nota->idasignatura == $atribucion->idasignatura)
                                 @if($nota->idtrimestre == '1')
                                    <td id="td1">{{ $nota->promedio }}</td>
                                 @endif
                                 @if($nota->idtrimestre == '2')
                                    <td id="td2">{{ $nota->promedio }}</td>
                                 @endif
                                 @if($nota->idtrimestre == '3')
                                    <td id="td3">{{ $nota->promedio }}</td>
                                 @endif
                              @endif
                           @endif
                       @endif
                   @endforeach
            </tr>
         @endif
         @endforeach
    </tbody>
</table>
</div>
<div style="float:left;width:20%">
<table class="table table-condensed">
    <thead>

      <th>Nota Final</th>
    </thead>
    <tbody>

            <tr>

                   @foreach($notas as $nota)
                       @if($nota->records->idgrado == $identificador->id)
                           @if($nota->records->idalumno == $alumno->id)
                              @if($nota->idasignatura == '1')
                                    <?php $suma = $suma + $nota->promedio;?>
                              @endif
                           @endif
                       @endif
                   @endforeach
                   <?php $suma=round($suma/3); ?>
                   <td>{{ $suma }}</td>
            </tr>
            <?php $suma=0; ?>

            <tr>

                   @foreach($notas as $nota)
                       @if($nota->records->idgrado == $identificador->id)
                           @if($nota->records->idalumno == $alumno->id)
                              @if($nota->idasignatura == '2')
                                    <?php $suma = $suma + $nota->promedio;?>
                              @endif
                           @endif
                       @endif
                   @endforeach
                   <?php $suma=round($suma/3); ?>
                   <td>{{ $suma }}</td>
            </tr>
            <?php $suma=0; ?>

            <tr>

                   @foreach($notas as $nota)
                       @if($nota->records->idgrado == $identificador->id)
                           @if($nota->records->idalumno == $alumno->id)
                              @if($nota->idasignatura == '3')
                                    <?php $suma = $suma + $nota->promedio;?>
                              @endif
                           @endif
                       @endif
                   @endforeach
                   <?php $suma=round($suma/3); ?>
                   <td>{{ $suma }}</td>
            </tr>
            <?php $suma=0; ?>
            <tr>
              
                   @foreach($notas as $nota)
                       @if($nota->records->idgrado == $identificador->id)
                           @if($nota->records->idalumno == $alumno->id)
                              @if($nota->idasignatura == '4')
                                    <?php $suma = $suma + $nota->promedio;?>
                              @endif
                           @endif
                       @endif
                   @endforeach
                   <?php $suma=round($suma/3); ?>
                   <td>{{ $suma }}</td>
            </tr>
            <?php $suma=0; ?>

    </tbody>
</table>
</div>
</div>
@endsection
