@extends('templates.main')

@section('title', 'Registro Docente')

@section('content')
	{!! Form::open(['route'=>'notas.store', 'method'=>'POST']) !!}
  <div class="form-group">
    {!! Form::label('idrecord', 'Alumnos') !!}
    {!! Form::select('idrecord', $alumnos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un Alumno','required']) !!}
  </div>
	<div class="form-group">
		{!! Form::label('tipo', 'Trimestre') !!}
    {!!  Form::text('tipo', '1')!!}
	</div>
	<div class="form-group">
		{!! Form::label('calificacion', 'Nota') !!}

		{!! Form::text('calificacion', null, ['class'=>'form-control', 'placeholder'=>'Escriba su Nota', 'required']) !!}
	</div>

  <div class="form-group">
    {!! Form::label('idasignatura', 'Listado de Asignaturas') !!}
    {!! Form::select('idasignatura', $asig, null, ['class'=>'form-control', 'placeholder'=>'Seleccione una materia','required']) !!}
  </div>



	<div class="form-group">
		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('docentes.index') }}" class="btn btn-info">Cancelar</a><hr>

	</div>
	{!! Form::close() !!}
@endsection
