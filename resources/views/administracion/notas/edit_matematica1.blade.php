@extends('templates.main')

@section('title', 'Registro de Notas por Materia')

@section('content')

    <h3 align="center">Notas de {{$identificador->nombre}}</h3>
    <h3 align="center">Tercer Trimestre</h3>
    <h3 align="center">Matemáticas</h3>


    <table class="table table-condensed">

        <thead>
          <th>ID</th>
          <th>ID_Record</th>
          <th>Trimestre</th>
          <th>Nota 1</th>
          <th>Nota 2</th>
          <th>Nota 3</th>
          <th>Promedio</th>
          <th>ID_Asignatura</th>


        </thead>

        <tbody>

            @foreach($notas as $nota)
                   <tr>
                     <td>{{ $nota->id }}</td>
                     <td>{{ $nota->idrecord }}</td>
                     <td>{{ $nota->tipo }}</td>
                     <td>{{ $nota->nota1 }}</td>
                     <td>{{ $nota->nota2 }}</td>
                     <td>{{ $nota->nota3 }}</td>
                     <td>{{ $nota->promedio }}</td>
                     <td>{{ $nota->idasignatura }}</td>

                  </tr>
             @endforeach

        </tbody>
	</table>
@endsection
