@extends('templates.main')

@section('title', 'Registro de Notas por Materia')

@section('content')

    <h3 align="center">Notas de {{$identificador->nombre}}</h3>
    <h3 align="center">{{ $asig->nombre }}</h3>
    <h3 align="center">Tercer Trimestre</h3>



    {!! Form::open(['route'=>['notas.update', $identificador], 'method'=>'PUT']) !!}
    <td align="center">
    <a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $identificador->id, "idasignatura" => $asig->id, "id" => '0']) }}"  class="btn btn-warning">
      <span class="glyphicon glyphicon-search"  aria-hidden="true">Primer_Trimestre</span>
    </a>
    </td>
    <td align="center">
    <a href="{{ url('administracion/notas/edit_trimestre', ["idgrado" => $identificador->id, "idasignatura" => $asig->id, "id" => '0']) }}"  class="btn btn-warning">
      <span class="glyphicon glyphicon-search"  aria-hidden="true">Segundo_Trimestre</span>
    </a>
    </td>

    <table class="table table-condensed">

        <thead>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th></th>
          <th></th>
          <th>nota 1</th>
          <th>nota 2</th>
          <th>nota 3</th>

          <th></th>

        </thead>

        <tbody>


            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->id > $i)
                   <tr>
                     <td style="width:100px;">{{ $registro->alumnos->nombres }}</td>
                     <td style="width:110px;">{{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>
  {!! Form::close() !!}

{!! Form::open(['route'=>'notas.store', 'method'=>'POST']) !!}
<td>
<div class="form-group">
  {!!  Form::hidden('idrecord', $registro->id)!!}
</div>
</td>
<td style="width:85px;">
<div class="form-group">
  {!!  Form::hidden('idtrimestre', '3')!!}
</div>
</td>
<td style="width:85px;">
<div class="form-group">
 {!! Form::text('nota1', null, ['class'=>'form-control', 'placeholder'=>'00.00']) !!}
</div>
</td>
<td style="width:85px;">
<div class="form-group">
 {!! Form::text('nota2', null, ['class'=>'form-control', 'placeholder'=>'00.00']) !!}
</div>
</td>
<td style="width:85px;">
<div class="form-group">
 {!! Form::text('nota3', null, ['class'=>'form-control', 'placeholder'=>'00.00']) !!}
</div>
</td>
<td>
<div class="form-group">
 {!! Form::hidden('promedio', '(nota1+nota2+nota3)/3') !!}
</div>
</td>
<td>
<div class="form-group">
  {!!  Form::hidden('idasignatura', $asig->id)!!}
</div>
</td>
                <td><div class="form-group">
                    		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
                    	</div></td>
{!! Form::close() !!}
<!--<td align="center">
<a href="{{ route('administracion.notas.edit_historial',["idgrado" => $registro->grados->id, "idalumno" => $registro->alumnos->id]) }}"  class="btn btn-warning">
  <span class="glyphicon glyphicon-search"  aria-hidden="true">Historial</span>
</a>
</td>-->
                  </tr>

								@endif
             @endif
             @endforeach

        </tbody>
	</table>




@endsection
