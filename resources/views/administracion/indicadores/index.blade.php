@extends('templates.main')

@section('title', 'Listado de Indicadores')

@section('content')
<a href="{{ route('indicadors.create') }}" class="btn btn-info">Registrar nuevo Indicador</a>
<a href="{{ route('indicadores.baja.index') }}" class="btn btn-info">Consultar Indicadores Inactivos</a>
<a href="{{ route('home') }}" class="btn btn-info">Inicio</a><hr>

 <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>Area</th>
            <th>Indicador</th>
            <th>Acción</th>
        </thead>
        <tbody>
            @foreach($indicadors as $indicador)

            @if ($indicador->area_indicador->activo == 1 and $indicador->activo == 1)
                <tr>
                    <td>{{ $indicador->area_indicador->nombre }}</td>
                    <td>{{ $indicador->nombre }}</td>
                    <td align="center"><a href="{{ route('administracion.indicadores.destroy', $indicador->id) }}"onclick="return confirm('¿Deseas dar de baja este Indicador?')" class="btn btn-danger"><span r"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    <a href="{{ route('indicadors.edit', $indicador->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    

@endsection
