@extends('templates.main')

@section('title', 'Listado de Indicadores Inactivos')

@section('content')
<a href="{{ route('indicadors.index') }}" class="btn btn-info">Consultar Indicadores Activos</a><hr>

   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>Area</th>
            <th>Indicador</th>
            <th>Acción</th>
        </thead>
        <tbody>
            @foreach($indicadores as $indicador)

            @if ($indicador->activo == 0)
                <tr>
                    <td>{{ $indicador->area_indicador->nombre }}</td>
                    <td>{{ $indicador->nombre }}</td>
                    <td align="center"><a href="{{ route('administracion.indicadores.alta', $indicador->id) }}" onclick="return confirm('¿Desea dar de alta este Indicador?')" class="btn btn-danger"><span r"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
   

@endsection