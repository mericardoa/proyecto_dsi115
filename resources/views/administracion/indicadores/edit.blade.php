@extends('templates.main')

@section('title', 'Actualización de datos de Indicador')

@section('content')
	{!! Form::open(['route'=>['indicadores.update', $indicadores], 'method'=>'PUT']) !!}
		<div class="form-group">
			{!! Form::label('id_area', 'Area Asignada') !!}
			{!! Form::select('id_area', $area_indicadores, $indicadores->area_indicador->id, ['class'=>'form-control', 'placeholder'=>'Seleccione un area','required']) !!}
		</div>
		<div class="form-group">
		{!! Form::label('nombre', 'Nombre de Indicador') !!}

		{!! Form::text('nombre', $indicadores->nombre, ['class'=>'form-control', 'placeholder'=>'Escriba el nombre del indicador', 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('Nivel', 'Nivel Asignado') !!}
			{!! Form::select('Nivel', [''=>'Selecione','5'=>'5 Años', '6'=>'6 Años'], $indicadores->Nivel,['class'=>'form-control']) !!}





		</div>

		<div class="form-group">
			{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
			<a href="{{ route('indicadors.index') }}" class="btn btn-info">Cancelar</a>
		</div>
	{!! Form::close() !!}
@endsection
