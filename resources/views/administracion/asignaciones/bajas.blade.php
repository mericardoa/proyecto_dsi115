@extends('templates.main')

@section('title', 'Asignacion de Grados a Docentes Inactivas')

@section('content')
  
    
    <a href="{{ route('asignaciones.index') }}" class="btn btn-info">Asiganciones Activas</a>
    <hr>
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>

            <th>Docente</th>
            <th>Grado</th>
            <th>Opciones</th>

        </thead>
        <tbody>
          @foreach($asignaciones as $asignacion)
           @if ($asignacion->activo == 0)
           <tr>
             <td>{{ $asignacion->docentes->nombres }} , {{ $asignacion->docentes->apellidos }}</td>
             <td>{{ $asignacion->grados->nombre }} ( {{ $asignacion->grados->turnos->turno }} )</td>
             <td align="center
             "><a href="{{ route('administracion.assignments.alta', $asignacion->id) }}" onclick="return confirm('¿Desea dar de alta a esta Asignacion?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a>

          </tr>
          @endif
         @endforeach

        </tbody>
	</table>
      
@endsection
