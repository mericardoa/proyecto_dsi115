@extends('templates.main')

@section('title', 'Actualización de asignaciones')

@section('content')
	
	{!! Form::open(['route'=>['asignaciones.update', $asignaciones], 'method'=>'PUT']) !!}
		
	<div class="form-group">
		{!! Form::label('iddocente', 'Lista de Docentes') !!}
		{!! Form::select('iddocente',  $docentes, $asignaciones->iddocente, ['class'=>'form-control', 'placeholder'=>'Seleccione un Docente', 'required']) !!}
	</div>

  <div class="form-group">
    {!! Form::label('idgrado', 'Lista de Grados') !!}
    {!! Form::select('idgrado',  $grados, $asignaciones->idgrado, ['class'=>'form-control', 'placeholder'=>'Seleccione un Grado', 'required']) !!}
  </div>


	<div class="form-group">
		{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('asignaciones.index') }}" class="btn btn-info">Cancelar</a>
	</div>
	{!! Form::close() !!}
@endsection



 