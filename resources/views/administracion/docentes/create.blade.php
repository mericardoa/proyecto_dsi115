@extends('templates.main')

@section('title', 'Registro Docente')

@section('content')
	{!! Form::open(['route'=>'docentes.store', 'method'=>'POST']) !!}
	<div class="form-group">
		{!! Form::label('nip', 'Número de Identificación Profesional') !!}

		{!! Form::text('nip', null, ['class'=>'form-control', 'placeholder'=>'#######', 'required',]) !!}
	</div>
	<div class="form-group">
		{!! Form::label('nombres', 'Nombres') !!}

		{!! Form::text('nombres', null, ['class'=>'form-control', 'placeholder'=>'Escriba sus nombres', 'required']) !!}
	</div>
	
	<div class="form-group">
		{!! Form::label('apellidos', 'Apellidos') !!}

		{!! Form::text('apellidos', null, ['class'=>'form-control', 'placeholder'=>'Escriba sus apellidos', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('dui', 'Documento Único de Identidad') !!}

		{!! Form::text('dui', null, ['class'=>'form-control', 'placeholder'=>'########-#', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('nit', 'Número de Identificación Tributaria') !!}

		{!! Form::text('nit', null, ['class'=>'form-control', 'placeholder'=>'####-######-###-#', 'required',]) !!}
	</div>

	<div class="form-group">
		{!! Form::label('especialidad', 'Especialidad') !!}

		{!! Form::text('especialidad', null, ['class'=>'form-control', 'placeholder'=>'Especialidad o grado académico del docente', 'required',]) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('docentes.index') }}" class="btn btn-info">Cancelar</a><hr>
		
	</div>
	{!! Form::close() !!}
@endsection