@extends('templates.main')

@section('title', 'Listado de Docentes')

@section('content')
    <a href="{{ route('docentes.create') }}" class="btn btn-info">Registrar nuevo docente</a>
    <a href="{{ route('badoc.index') }}" class="btn btn-info">Consultar Docentes Inactivos</a>
    <a href="{{ route('home') }}" class="btn btn-info">Inicio</a>
    <hr>

<div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>NIP</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>DUI</th>
			<th>NIT</th>
			<th>Especialidad</th>
			<th>Opciones</th>
        </thead>
        <tbody>
            @foreach($docentes as $docente)
            @if ($docente->activo == 1)
                <tr>
                
                	<td>{{ $docente->nip }}</td>
                	<td>{{ $docente->nombres }}</td>
                	<td>{{ $docente->apellidos }}</td>
                	<td>{{ $docente->dui }}</td>
                	<td>{{ $docente->nit }}</td>
                	<td>{{ $docente->especialidad }}</td>
                	<td><a href="{{ route('administracion.docentes.destroy', $docente->id) }}" onclick="return confirm('¿Deseas dar de baja este Docente?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a> 
                    <a href="{{ route('docentes.edit', $docente->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
                </tr>
                @endif
            @endforeach
        </tbody>
	</table>


@endsection