@extends('templates.main')

@section('title', 'Listado de Docentes')

@section('content')
    <a href="{{ route('docentes.index') }}" class="btn btn-info">Ver Docentes Activos</a>
    <hr>
    
    <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>NIP</th>
			<th>Nombres</th>
			<th>Apellidos</th>
			<th>DUI</th>
			<th>NIT</th>
			<th>Especialidad</th>
			<th>Acción</th>
        </thead>
        <tbody>
            @foreach($docentes as $docente)
            @if ($docente->activo == 0)
                <tr>
                
                	<td>{{ $docente->nip }}</td>
                	<td>{{ $docente->nombres }}</td>
                	<td>{{ $docente->apellidos }}</td>
                	<td>{{ $docente->dui }}</td>
                	<td>{{ $docente->nit }}</td>
                	<td>{{ $docente->especialidad }}</td>
                	<td align="center"><a href="{{ route('administracion.docentes.alta', $docente->id) }}" onclick="return confirm('¿Deseas dar de alta este Docente?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a> 
                   </td>
                </tr>
                @endif
            @endforeach
        </tbody>
	</table>

@endsection