@extends('templates.main')

@section('title', 'Busqueda de Alumnos por Grado')

@section('content')
    <a href="{{ route('registros.index') }}" class="btn btn-info">Regresar</a>

    <!--BUSCADOR DE ALUMNOS POR GRADO -->
         {!! Form::open(['route' => 'registros.show', '$registros->idgrado', 'method' => 'POST', 'class' => 'navbar-form pull-right']) !!}

                <div class="input-group input-group-lg">
                    {!! Form::text('idgrado', null, ['class' => 'form-control', 'placelholder' => 'Buscar Grado...', 'aria-describedby' => 'buscar' ]) !!}
                      <span class="input-group-addon" id="buscar"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                </div>

         {!! Form::close() !!}
    <!--FIN DEL BUSCADOR -->

     <hr>
    <table class="table table-condensed">
        <thead>

            <th>Alumno</th>
            <th>Grado (Docente)</th>
            <th>Fecha</th>
            <th>Opciones</th>

        </thead>
        <tbody>
          @foreach($registros as $registro)
          <tr>
             <td>{{ $registro->alumnos->nombres }}, {{ $registro->alumnos->apellidos }}</td>
             <td>{{ $registro->assignments->grados->nombre }}, ({{ $registro->assignments->docentes->nombres }}) </td>
             <td>{{ $registro->año }}</td>
             <td align="center
             "><a href="{{ route('administracion.registros.destroy', $registro->id) }}" onclick="return confirm('¿Deseas eliminar una Asignacion?')" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                    <a href="{{ route('registros.edit', $registro->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
         </tr>
         @endforeach

        </tbody>
	</table>
    {{$registros->render()}}
@endsection
