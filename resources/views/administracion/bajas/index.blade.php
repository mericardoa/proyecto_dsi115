@extends('templates.main')

@section('title', 'Listado de Alumnos')

@section('content')
    <a href="{{ route('alumnos.create') }}" class="btn btn-info">Registrar nuevo alumno</a>
     <a href="{{ route('home') }}" class="btn btn-info">Inicio</a><hr>
     <hr>
     
    <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover table table-condensed">
      <thead class="thead-default">
      <th>Nombres</th>
			<th>Apellido Padre</th>
			<th>Apellido Madre</th>
      <th>Sexo</th>
      <th>Fecha Nacimiento</th>
      <th>Repite</th>
      <th>Estudio Parvularia</th>
			<th>Zona Residencia</th>
      <th>Codigo Departamento</th>
      <th>Actividad Economica</th>
      <th>Tipos Discapacidad</th>
      <th>Encargado</th>
      <th>Direccion</th>
      <th>Opciones</th>
       </thead>


        <tbody>
                     @foreach($alumnos as $alumno)
                <tr>
                  @if ($alumno->activo == 0)
                  <td>{{ $alumno->nombres }}</td>
                  <td>{{ $alumno->apellido_padre }}</td>
                  <td>{{ $alumno->apellido_madre }}</td>
                  <td>{{ $alumno->sexo }}</td>
                  <td>{{ $alumno->fecha_nacimiento }}</td>
                  <td>{{ $alumno->repite }}</td>
                  <td>{{ $alumno->estudio_parvularia }}</td>
                  <td>{{ $alumno->zona_residencia }}</td>
                  <td>{{ $alumno->cod_depto_residencia }}</td>
                  <td>{{ $alumno->actividad_econ }}</td>
                  <td>{{ $alumno->tipos_discapacidad }}</td>
                  <td>{{ $alumno->encargado }}</td>
                  <td>{{ $alumno->direccion }}</td>
                  

                    <td><a href="{{ route('administracion.alumnos.destroy', $alumno->id) }}" onclick="return confirm('¿Deseas dar de baja  este Alumno?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    <a href="{{ route('alumnos.edit', $alumno->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
                </tr>
                @endif
            @endforeach
        </tbody>
	</table>
  {{$alumnos->render()}}
</div>

@endsection
