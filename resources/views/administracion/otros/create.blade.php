@extends('templates.main')

@section('title', 'Grados Asignados')

@section('content')


    <hr>

    <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>Grados</th>
            <th>Registros Notas</th>
            <th>Historial Notas</th>
            <!--<th align="center">Registro Notas</th> -->

        </thead>
        <tbody>
          @foreach($registros as $registro)
             @if($registro->iddocente == Auth::user()->iddocente)
             <tr>
                 <td>{{ $registro->grados->nombre }}</td>
                 <td>
                 @foreach($atribuciones as $atribucion)
                   @if($registro->idgrado == $atribucion->idgrado)
                      @if($atribucion->asignaturas->id == 1)
                             <li><a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $registro->grados->id, "idasignatura" => '1', "id" => '0']) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>
                      @elseif($atribucion->asignaturas->id == 2)
                             <li><a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $registro->grados->id, "idasignatura" => '2', "id" => '0']) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>
                      @elseif($atribucion->asignaturas->id == 3)
                             <li><a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $registro->grados->id, "idasignatura" => '3', "id" => '0']) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>
                      @elseif($atribucion->asignaturas->id == 4)
                             <li><a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $registro->grados->id, "idasignatura" => '4', "id" => '0']) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>
                      @elseif($atribucion->asignaturas->id == 5)
                            <li><a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $registro->grados->id, "idasignatura" => '5', "id" => '0']) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>
                      @elseif($atribucion->asignaturas->id == 6)
                            <li><a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $registro->grados->id, "idasignatura" => '6', "id" => '0']) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>
                      @else
                            <li><a href="{{ url('administracion/notas/edit_matematica', ["idgrado" => $registro->grados->id, "idasignatura" => '7', "id" => '0']) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>
                      @endif

                  <!--  <li><a href="{{ route('notas.edit', $registro->grados->id) }}" class="btn btn-link">{{ $atribucion->asignaturas->nombre }}</a></li>-->
                    @endif

                 @endforeach
                 <li><a href="{{ url('administracion/conductas/editar', ["idgrado" => $registro->grados->id]) }}" class="btn btn-link">Conducta</a></li>
               </td>

                 <td align="center">
                 <a href="{{ route('otros.show', $registro->grados->id) }}"  class="btn btn-primary">
                   <span class="glyphicon glyphicon-search"  aria-hidden="true"></span>
                 </a>
                 </td>

              <!--   <td align="center">
                 <a href="{{ route('notas.edit', $registro->grados->id) }}"  class="btn btn-primary">
                   <span class="glyphicon glyphicon-list"  aria-hidden="true"></span>
                 </a>
               </td>-->

             </tr>
             @endif

             @endforeach

        </tbody>
	</table>
     {{$registros->render()}}

     <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" /></script>

@endsection
