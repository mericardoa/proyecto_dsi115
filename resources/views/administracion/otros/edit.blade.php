@extends('templates.main')

@section('title', 'Asignacion de Alumno a Grado')

@section('content')
	{!! Form::open(['route'=>['otros.update', $identificador], 'method'=>'PUT']) !!}
  <div class="form-group">
    {!! Form::label('idalumno', 'Listado de alumnos') !!}
    {!! Form::select('idalumno', $alumnos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un Alumno','required']) !!}
  </div>
	<div class="form-group">
		{!! Form::label('', '') !!}

		{!! Form::hidden('idgrado', $identificador->id, ['class'=>'form-control', 'placeholder'=>'Escriba sus nombres', 'required']) !!}
	</div>



	<div class="form-group">
		{!! Form::submit('Asignar', ['class'=>'btn btn-primary']) !!}

	</div>
	{!! Form::close() !!}

     <hr>
    <h3>Datos de Alumnos de: {{$identificador->nombre}}</h3>
    <table class="table table-condensed">

        <thead>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Opciones</th>


        </thead>

        <tbody>

            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->activo == 1)
                   <tr>
                     <td>{{ $registro->alumnos->nombres }}</td>
                     <td>{{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>

                     <td>
                       <a href="{{ route('administracion.otros.destroy', $registro->id) }}" onclick="return confirm('¿Deseas dar de baja a este Registro?')" class="btn btn-danger">
                        <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
                       </a>
                       <a href="{{ route('registros.edit', $registro->id) }}"  class="btn btn-warning">
                        <span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a>
                    </td>
                  </tr>
								@endif
             @endif
             @endforeach

        </tbody>
	</table>
@endsection
