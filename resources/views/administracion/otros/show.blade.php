@extends('templates.main')

@section('title', 'Listado de Alumnos')

@section('content')

     <hr>
    <h3>Alumnos de: {{$identificador->nombre}}</h3>
    <table class="table table-condensed">

        <thead>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Trimestres</th>
          <th>Finales</th>

        </thead>

        <tbody>

            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->activo == 1)
                   <tr>
                     <td>{{ $registro->alumnos->nombres }}</td>
                     <td>{{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>

                     <td>
												<a href="{{ route('administracion.notas.edit_historial',["idgrado" => $registro->grados->id, "idalumno" => $registro->alumnos->id]) }}"  class="btn btn-primary">
												  <span class="glyphicon glyphicon-list"  aria-hidden="true"></span>
												</a>
                    </td>
                    <td>
                      <a href="{{ route('administracion.notas.edit_final',["idgrado" => $registro->grados->id, "idalumno" => $registro->alumnos->id]) }}"  class="btn btn-primary">
                        <span class="glyphicon glyphicon-list-alt"  aria-hidden="true"></span>
                      </a>
                    </td>
                  </tr>
								@endif
             @endif
             @endforeach

        </tbody>
	</table>
@endsection
