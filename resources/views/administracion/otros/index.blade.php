@extends('templates.main')

@section('title', 'Grados Asignados')

@section('content')
    <a href="{{ route('alumnos.create') }}" class="btn btn-info">Matricular un nuevo Alumno</a>

    <hr>

    <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>Grados</th>
            <th>Turno</th>
            <th align="center">Asignaciones</th>
            <!--<th align="center">Registro Notas</th> -->

        </thead>
        <tbody>
          @foreach($registros as $registro)
             @if($registro->iddocente == Auth::user()->iddocente)
             <tr>
                 <td>{{ $registro->grados->nombre }}</td>
                 <td>{{ $registro->grados->turnos->turno }}</td>

                 <td align="center">
                 <a href="{{ route('otros.edit', $registro->grados->id) }}"  class="btn btn-warning">
                   <span class="glyphicon glyphicon-search"  aria-hidden="true"></span>
                 </a>
                 </td>

              <!--   <td align="center">
                 <a href="{{ route('notas.edit', $registro->grados->id) }}"  class="btn btn-primary">
                   <span class="glyphicon glyphicon-list"  aria-hidden="true"></span>
                 </a>
               </td>-->

             </tr>
             @endif

             @endforeach

        </tbody>
	</table>
     {{$registros->render()}}

     <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" /></script>

@endsection
