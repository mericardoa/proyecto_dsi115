@extends('templates.main')

@section('title', 'Listado de Asignaturas')

@section('content')
    <a href="{{ route('asignaturas.create') }}" class="btn btn-info">Registrar nueva Asignatura</a>
     <a href="{{ route('asignaturas.baja.index') }}" class="btn btn-info">Consultar Asignaturas Inactivos</a>
    <a href="{{ route('home') }}" class="btn btn-info">Inicio</a>
    <hr>
       
    <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>Nombre</th>			
            <th>Opciones</th> 
        </thead>
        <tbody>
        
         @foreach($asignaturas as $asignatura)
         @if ($asignatura->activo == 1)

                <tr>

                <td>{{ $asignatura->nombre }}</td>

                	<td align="right"><a href="{{ route('administracion.asignaturas.destroy', $asignatura->id) }}" onclick="return confirm('¿Desea dar de baja a esta asignatura?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a> 
                    <a href="{{ route('asignaturas.edit', $asignatura->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
                </tr>
                @endif
            @endforeach
           
        </tbody>
	</table>

</div>
 
@endsection