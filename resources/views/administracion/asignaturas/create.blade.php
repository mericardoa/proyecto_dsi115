@extends('templates.main')

@section('title', 'Registro Asignatura')

@section('content')
	{!! Form::open(['route'=>'asignaturas.store', 'method'=>'POST']) !!}
	<div class="form-group">
		{!! Form::label('nombre', 'Nombre de Asignatura') !!}

		{!! Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Nombre de Asignatura', 'required',]) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('asignaturas.index') }}" class="btn btn-info">Cancelar</a>

	</div>

	{!! Form::close() !!}
@endsection
