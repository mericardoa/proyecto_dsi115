@extends('templates.main')

@section('title', 'Actualización de Asignaturas')

@section('content')
	{!! Form::open(['route'=>['asignaturas.update', $asignatura], 'method'=>'PUT']) !!}
	<div class="form-group">
		{!! Form::label('nombre', 'Nombre de la Asignatura ') !!}

		{!! Form::text('nombre', $asignatura->nombre, ['class'=>'form-control', 'placeholder'=>'#######', 'required',]) !!}
	</div>
	
	<div class="form-group">
		{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('asignaturas.index') }}" class="btn btn-info">Cancelar</a>
	</div>
	{!! Form::close() !!}
@endsection