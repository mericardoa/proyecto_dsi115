@extends('templates.main')

@section('title', 'Listado de Asignaturas Inactivas')

@section('content')
    <a href="{{ route('asignaturas.index') }}" class="btn btn-info">Asignaturas Activas</a>
    
    <hr>
       
    <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>Nombre</th>         
            <th>Opciones</th> 
        </thead>
        <tbody>
        
         @foreach($asignaturas as $asignatura)
         @if ($asignatura->activo == 0)

                <tr>

                <td>{{ $asignatura->nombre }}</td>

                    <td align="center"><a href="{{ route('administracion.asignaturas.alta', $asignatura->id) }}" onclick="return confirm('¿Desea dar de alta esta asignatura?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a> 
                    </td>
                </tr>
                @endif
            @endforeach
           
        </tbody>
    </table>

</div>
 
@endsection