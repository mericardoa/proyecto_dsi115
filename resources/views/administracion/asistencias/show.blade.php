@extends('templates.main')

@section('title', 'Estadistica de Asistencia')

@section('content')

 <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

        <thead>          
          <th>Alumno</th>
          <th>Asistio</th>
           <th>Permiso</th>
             <th>Sin Permiso</th>
               <th>Asueto</th>
        

        </thead>

        <tbody>
          <?php $suma0=0; ?>
           <?php $suma1=0; ?> 
            <?php $suma2=0; ?> 
             <?php $suma3=0; ?> 
              <?php $suma4=0; ?> 

            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->activo == 1)
                   <tr>
                    
                    <td>{{ $registro->alumnos->nombres }} {{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>
                     
                    @foreach($asistencias as $asistencia)
                    @if($registro->id == $asistencia->idrecord)
                     @if($registro->idalumno == $asistencia->records->idalumno)
                    
                      
                       
                        @if($asistencia->estado == 'Asistio')
                         
                          <?php $suma1++;?>
                          
                        @elseif($asistencia->estado == 'Permiso')
                          <?php $suma2++;?>
                            
                        @elseif($asistencia->estado == 'Sin Permiso')
                          
                           <?php $suma3++;?>
                       

                         @else
                         
                           <?php $suma4++;?>
                       
                           
                        @endif
                      

                     @endif
                     @endif
                     @endforeach

                      
                      <td> {{ $suma1}}</td>
                      <td> {{ $suma2}}</td>
                      <td> {{ $suma3}}</td>
                      <td> {{ $suma4}}</td>
                      <?php $suma1=0; ?> 
            <?php $suma2=0; ?> 
             <?php $suma3=0; ?> 
              <?php $suma4=0; ?> 

                      
                  </tr>
								@endif
             @endif
             @endforeach

        </tbody>


	</table>
 
                   <a href="{{ route('asistencias.index') }}" class="btn btn-info">Cancelar</a>
                 
                 </a>

      
      
    
  </div>


@endsection