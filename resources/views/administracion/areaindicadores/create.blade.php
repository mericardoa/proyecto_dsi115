@extends('templates.main')

@section('title', 'Registro de Area de Indicadores')

@section('content')
	{!! Form::open(['route'=>'area_indicadores.store', 'method'=>'POST']) !!}

	<div class="form-group">
		{!! Form::label('nombre', 'Area de Indicador') !!}

		{!! Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Ingrese el nombre del Area', 'required',]) !!}
	</div>
	<div class="form-group">
		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('area_indicadores.index') }}" class="btn btn-info right">Cancelar</a>

	</div>
	{!! Form::close() !!}
@endsection
