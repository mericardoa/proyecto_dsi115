@extends('templates.main')

@section('title', 'Actualización de datos de Area')

@section('content')
	{!! Form::open(['route'=>['area_indicadores.update', $area_indicadores], 'method'=>'PUT']) !!}
	<div class="form-group">
		{!! Form::label('nombre', 'Nombre de Area') !!}

		{!! Form::text('nombre', $area_indicadores->nombre, ['class'=>'form-control', 'placeholder'=>'Escriba el nombre del area', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('area_indicadores.index') }}" class="btn btn-info right">Cancelar</a>
		  
	</div>
	{!! Form::close() !!}
@endsection