@extends('templates.main')

@section('title', 'Listado de Usuarios')

@section('content')
<a href="{{ route('usuarios.create') }}" class="btn btn-info">Registrar nuevo usuario</a>
<a href="{{ route('usuarios.baja.index') }}" class="btn btn-info">Consultar Usuarios Inactivos</a>
  <a href="{{ route('home') }}" class="btn btn-info">Inicio</a><hr>


     <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>Docente</th>
            <th>Correo electrónico</th>
            <th>Rol</th>
            <th>Acción</th>
        </thead>
        <tbody>
            @foreach($users as $user)
            @if ($user->activo == 1)
                <tr>
                    <td>{{ $user->docente->nombres }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->rol }}</td>
                    <td><a href="{{ route('administracion.usuarios.destroy', $user->id) }}" onclick="return confirm('¿Deseas dar de baja este usuario?')"class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    <a href="{{ route('usuarios.edit', $user->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>

            </div>
            </div>
          


    {{$users->render()}}



@endsection
