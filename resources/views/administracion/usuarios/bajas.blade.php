@extends('templates.main')

@section('title', 'Listado de Usuarios Inactivos')

@section('content')
<a href="{{ route('usuarios.index') }}" class="btn btn-info">Ver Usuarios Activos</a>

<hr>

   <div class="table-responsive">
   <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>Docente</th>
            <th>Correo electrónico</th>
            <th>Rol</th>
            <th>Acción</th>
        </thead>
        <tbody>
            @foreach($users as $user)
            @if ($user->activo == 0)
                <tr>
                
                    <td>{{ $user->docente->nombres }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->rol }}</td>
                    <td><a href="{{ route('administracion.usuarios.alta', $user->id) }}" onclick="return confirm('¿Deseas dar de alta este usuario?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></a></td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    {{$users->render()}}



@endsection