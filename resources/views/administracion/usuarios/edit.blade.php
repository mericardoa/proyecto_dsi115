@extends('templates.main')

@section('title', 'Actualización de datos de Usuario')

@section('content')
	{!! Form::open(['route'=>['usuarios.update', $user], 'method'=>'PUT']) !!}
		<div class="form-group">
			{!! Form::label('iddocente', 'Docente asignado') !!}
			{!! Form::select('iddocente', $docentes, $user->docente->id, ['class'=>'form-control', 'placeholder'=>'Seleccione un docente','required']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('email', 'Correo electrónico') !!}
			{!! Form::email('email', $user->email, ['class'=>'form-control', 'placeholder'=>'example@gmail.com', 'required']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('rol', 'Rol') !!}
			{!! Form::select('rol', [''=>'Selecione','Director'=>'Director', 'Docente'=>'Docente', 'Docente Parvularia'=>'Docente Parvularia'], $user->rol, ['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('password', 'Contraseña') !!}
			{!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'******', 'required']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
			<a href="{{ route('usuarios.index') }}" class="btn btn-info">Cancelar</a>
		</div>
	{!! Form::close() !!}
@endsection
