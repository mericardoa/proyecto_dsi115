@extends('templates.main')

@section('title', 'Registro Grado')

@section('content')
	{!! Form::open(['route'=>'grados.store', 'method'=>'POST']) !!}

	<div class="form-group">
		{!! Form::label('idturno', 'Turno del Grado') !!}
		{!! Form::select('idturno',  $turnos , null, ['class'=>'form-control', 'placeholder'=>'Turnos', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('nombre', 'Grado') !!}

		{!! Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'#######', 'required',]) !!}
	</div>



  <!--<div class="form-group">
		{!! Form::label('idturno', 'Turno del Grado') !!}

		{!! Form::text('idturno', null, ['class'=>'form-control', 'placeholder'=>'#######', 'required',]) !!}
	</div>-->



	<div class="form-group">
		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('grados.index') }}" class="btn btn-info">Cancelar</a>

	</div>

	{!! Form::close() !!}
@endsection
