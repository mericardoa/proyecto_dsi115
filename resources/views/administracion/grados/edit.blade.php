@extends('templates.main')

@section('title', 'Actualización de Grados')

@section('content')
	{!! Form::open(['route'=>['grados.update', $grado], 'method'=>'PUT']) !!}

	<div class="form-group">
		{!! Form::label('idturno', 'Turno del Grado') !!}
		{!! Form::select('idturno',  $turnos , null, ['class'=>'form-control', 'placeholder'=>'Turnos', 'required']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('nombre', 'Nombre de la Asignatura ') !!}

		{!! Form::text('nombre', $grado->nombre, ['class'=>'form-control', 'placeholder'=>'#######', 'required',]) !!}
	</div>


	<div class="form-group">
		{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('grados.index') }}" class="btn btn-info">Cancelar</a>
	</div>
	
	{!! Form::close() !!}
@endsection
