@extends('templates.main')

@section('title', 'Listado de Grados Inactivos')

@section('content')
    
    <a href="{{ route('grados.index') }}" class="btn btn-info">Grados Activos</a>
    <hr>
  
    
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>

            <th>Grado</th>
            <th>Turno</th>
            <th align="left">Opciones</th>

        </thead>
        <tbody>

          @foreach($grados as $grado)
          @if ($grado->activo==0)
                 <tr>
                 
                 	<td>{{ $grado->nombre }}</td>
                  <td>{{ $grado->turnos->turno}}</td>

                  <td align="center"><a href="{{ route('administracion.grados.alta', $grado->id) }}" onclick="return confirm('¿Deseas dar de alta este Grado?')" class="btn btn-danger"><span r"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                   </td>

                 </tr>
                 @endif
             @endforeach

        </tbody>
	</table>
</div>
  
@endsection