@extends('templates.main')

@section('title', 'Listado de Grados')

@section('content')
    <a href="{{ route('grados.create') }}" class="btn btn-info">Registrar un nuevo Grado</a>
    <a href="{{ route('grados.baja.index') }}" class="btn btn-info">Consultar Grados Inactivos</a>
    <a href="{{ route('home') }}" class="btn btn-info">Inicio</a>


    <hr>
  
    
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>

            <th>Grado</th>
            <th>Turno</th>
            <th align="left">Opciones</th>

        </thead>
        <tbody>

          @foreach($grados as $grado)
          @if ($grado->activo==1)
                 <tr>
                 
                 	<td>{{ $grado->nombre }}</td>
                  <td>{{ $grado->turnos->turno}}</td>

                  <td align="center"><a href="{{ route('administracion.grados.destroy', $grado->id) }}" onclick="return confirm('¿Deseas dar de baja Grado?')" class="btn btn-danger"><span r"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    <a href="{{ route('grados.edit', $grado->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>

                 </tr>
                 @endif
             @endforeach

        </tbody>
	</table>
</div>
  
@endsection
