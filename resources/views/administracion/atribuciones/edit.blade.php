@extends('templates.main')

@section('title', 'Actualización de asignaciones')

@section('content')
	{!! Form::open(['route'=>['atribuciones.update', $atribuciones], 'method'=>'PUT']) !!}

        <div class="form-group">
			{!! Form::label('idgrado', 'Listado de Grados') !!}
			{!! Form::select('idgrado', $grados, $atribuciones->idgrado,['class'=>'form-control', 'placeholder'=>'Seleccione un Grado','required']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('idasignatura', 'Listado de Asignaturas') !!}
			{!! Form::select('idasignatura', $asignaturas, $atribuciones->idasignatura, ['class'=>'form-control' , 'placeholder'=>'Seleccione una Asignatura','required']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
			<a href="{{ route('atribuciones.index') }}" class="btn btn-info">Atras</a>
		</div>
	{!! Form::close() !!}
@endsection