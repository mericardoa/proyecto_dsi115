@extends('templates.main')

@section('title', 'Asignaciones de Grados a Asignaturas Inactivas')

@section('content')
    <a href="{{ route('atribuciones.index') }}" class="btn btn-info">Asignaciones Activas</a>
    
     <hr>
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>

            <th>Grado</th>
            <th>Asignatura</th>

            <th>Opciones</th>

        </thead>
        <tbody>
          @foreach($atribuciones as $atribucion)
          @if ($atribucion->activo == 0)
           <tr>
             <td>{{ $atribucion->grados->nombre }}</td>
             <td>{{ $atribucion->asignaturas->nombre }} </td>

             <td align="center
             "><a href="{{ route('administracion.atribuciones.alta', $atribucion->id) }}" onclick="return confirm('¿Deseas dar de alta esta Asignacion?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                   </td>
           </tr>
          @endif
         @endforeach

        </tbody>
	</table>
        
@endsection