@extends('templates.main')

@section('title', 'Asignacion de Grados a Asignaturas')

@section('content')
    <a href="{{ route('atribuciones.create') }}" class="btn btn-info">Asignar una Asignatura</a>
    <a href="{{ route('atribuciones.baja.index') }}" class="btn btn-info">Consultar Asignaciones Inactivas</a>
     <a href="{{ route('home') }}" class="btn btn-info">Inicio</a>
     <hr>
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>

            <th>Grado</th>
            <th>Asignatura</th>

            <th>Opciones</th>

        </thead>
        <tbody>
          @foreach($atribuciones as $atribucion)
          @if ($atribucion->activo == 1)
           <tr>
             <td>{{ $atribucion->grados->nombre }}</td>
             <td>{{ $atribucion->asignaturas->nombre }} </td>

             <td align="center
             "><a href="{{ route('administracion.atribuciones.destroy', $atribucion->id) }}" onclick="return confirm('¿Deseas dar de baja a esta Asignacion?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    <a href="{{ route('atribuciones.edit', $atribucion->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
           </tr>
          @endif
         @endforeach

        </tbody>
	</table>
        
@endsection
