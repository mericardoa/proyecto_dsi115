<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'administracion', 'middleware'=>'auth'], function(){
	Route::resource('docentes', 'DocentesController');
	Route::get('docentes/destroy/{id}', [
			'uses'=> 'DocentesController@destroy',
			'as'  => 'administracion.docentes.destroy'
	]);

	Route::resource('usuarios', 'UsersController');
	Route::get('usuarios/destroy/{id}', [
			'uses'=> 'UsersController@destroy',
			'as'  => 'administracion.usuarios.destroy'
	]);
   Route::get('usuarios/alta/{id}', [
      'uses'=> 'UsersController@altau',
      'as'  => 'administracion.usuarios.alta'
  ]);
  Route::get('usuarios/bajas/index', [
      'uses'=> 'UsersController@bajasuindex',
      'as'  => 'usuarios.baja.index'
  ]);


//RUTAS PARA ASISTENCIA

  Route::resource('asistencias', 'AsistenciasController');
  Route::get('asistencias/destroy/{id}', [
      'uses'=> 'AsistenciasController@destroy',
      'as'  => 'administracion.asistencias.destroy'
  ]);
   
  Route::get('ingresar-asistencia',[
      'uses'=> 'AsistenciasController@insertarAsistencia',
      'as'  => 'administracion.asistenciasmultiple'
  ]);

/*Route::get('multiple-inserting',[
      'uses'=> 'IndAssingController@multipleInsert',
      'as'  => 'administracion.indicador_assignment.multiple-insert'
  ]); */ 


//RUTA PARA DAR DE ALTA A DOCENTE
  Route::get('docentes/alta/{id}', [
      'uses'=> 'DocentesController@altad',
      'as'  => 'administracion.docentes.alta'
  ]);

  Route::get('assignments/alta/{id}', [
      'uses'=> 'AssignmentsController@alta',
      'as'  => 'administracion.assignments.alta'
  ]);



	Route::resource('asignaturas', 'AsignaturasController');
	Route::get('asignaturas/destroy/{id}', [
			'uses'=> 'AsignaturasController@destroy',
			'as'  => 'administracion.asignaturas.destroy'
	]);
  Route::get('asignaturas/alta/{id}', [
      'uses'=> 'AsignaturasController@altaasig',
      'as'  => 'administracion.asignaturas.alta'
  ]);
  Route::get('asignaturas/bajas/index', [
      'uses'=> 'AsignaturasController@bajasasigindex',
      'as'  => 'asignaturas.baja.index'
  ]);

//Rutas del alumno
  Route::resource('alumnos', 'AlumnosController');
	Route::get('alumnos/destroy/{id}', [
			'uses'=> 'AlumnosController@destroy',
			'as'  => 'administracion.alumnos.destroy'
	]);
  Route::get('alumnos/alta/{id}', [
      'uses'=> 'AlumnosController@alta',
      'as'  => 'administracion.alumnos.alta'
  ]);
  Route::get('alumnos/bajas/index', [
      'uses'=> 'AlumnosController@bajasindex',
      'as'  => 'alumnos.baja.index'
  ]);

  //Rutas de los grados
  Route::resource('grados', 'GradosController');
	Route::get('grados/destroy/{id}', [
			'uses'=> 'GradosController@destroy',
			'as'  => 'administracion.grados.destroy'

	]);

   Route::get('grados/alta/{id}', [
      'uses'=> 'GradosController@altag',
      'as'  => 'administracion.grados.alta'
  ]);
  Route::get('grados/bajas/index', [
      'uses'=> 'GradosController@bajasgindex',
      'as'  => 'grados.baja.index'
  ]);

//PARA AREA INDICADORES MERINO
//*******************************************************************************************
//rutas para areaINDICADORES
  Route::resource('area_indicadores', 'AreaIndicadorController');
  Route::get('area_indicadores/destroy/{id}', [
      'uses'=> 'AreaIndicadorController@destroy',
      'as'  => 'administracion.area_indicadores.destroy'
  ]);


  Route::get('area_indicadores/alta/{id}', [
      'uses'=> 'AreaIndicadorController@altaai',
      'as'  => 'administracion.area_indicadores.alta'
  ]);
  Route::get('area_indicadores/bajas/index', [
      'uses'=> 'AreaIndicador@bajasai',
      'as'  => 'area_indicadores.baja.index'
  ]);


  Route::get('area_indicadores/bajas/index', [
      'uses'=> 'AreaIndicadorController@bajasaiindex',
      'as'  => 'area_indicadores.baja.index'
  ]);


 Route::put('area_indicadores/update/{id}', [
      'uses'=> 'DarBajaController@destroy',
      'as'  => 'administracion.area_indicadores.update1'
  ]);


 //Rutas para indicadores

 Route::resource('indicadores', 'IndicadorController');
  Route::get('indicadores/destroy/{id}', [
      'uses'=> 'IndicadorController@destroy',
      'as'  => 'administracion.indicadores.destroy'
  ]);

   Route::get('indicadores/alta/{id}', [
      'uses'=> 'IndicadorController@altai',
      'as'  => 'administracion.indicadores.alta'
  ]);
  
  Route::get('indicadores/bajas/index', [
      'uses'=> 'IndicadorController@bajasi',
      'as'  => 'indicadores.baja.index'
  ]);


  Route::get('indicadores/bajas/index', [
      'uses'=> 'IndicadorController@bajasiindex',
      'as'  => 'indicadores.baja.index'
  ]);

//******************************************************************************************

  //Ruta para Mostrar Entidades de bajas

Route::get('baja_alumno/index', [
      'uses'=> 'BajasController@bajasalumnos',
      'as'  => 'balumn.index'
  ]);

Route::get('baja_docente/index', [
      'uses'=> 'BajasController@bajasdocentes',
      'as'  => 'badoc.index'
  ]);
//Rutas que muestran el index de las bajas .
  Route::get('baja_asignaciones/index', [
        'uses'=> 'BajasController@bajasassignments',
        'as'  => 'basig.index'
    ]);



  Route::get('altas_asignaciones/index', [
          'uses'=> 'BajasController@altasassignments',
          'as'  => 'alsig.index'
    ]);

  Route::get('baja_registros/index', [
        'uses'=> 'BajasController@bajasrecords',
        'as'  => 'bareg.index'
    ]);Route::get('baja_atribuciones/index', [
          'uses'=> 'BajasController@bajasattributions',
          'as'  => 'batri.index'
      ]);

  Route::get('baja/baja/{id}', [
      'uses'=> 'BajasController@baja',
      'as'  => 'administracion.bajas.baja'
      ]);
    Route::resource('baja', 'BajasController');


Route::resource('bajadoc', 'BDocController');
  Route::get('bajadoc/baja/{id}', [
      'uses'=> 'BDocController@baja',
      'as'  => 'administracion.bajadoc.baja'
  ]);


  Route::resource('asignaciones', 'AssignmentsController');
  Route::get('asignaciones/destroy/{id}', [
      'uses'=> 'AssignmentsController@destroy',
      'as'  => 'administracion.asignaciones.destroy'
  ]);

  Route::resource('registros', 'RecordsController');
  Route::get('registros/destroy/{id}', [
      'uses'=> 'RecordsController@destroy',
      'as'  => 'administracion.registros.destroy'
  ]);

  //*******************************************
  //*******************************************

  //RUTAS DE ATRIBUCIONES ASIGNACION DE GRADOS A ASIGNATURA 

  Route::resource('atribuciones', 'AttributionsController');
  Route::get('atribuciones/destroy/{id}', [
      'uses'=> 'AttributionsController@destroy',
      'as'  => 'administracion.atribuciones.destroy'
  ]);

 Route::get('atribuciones/alta/{id}', [
      'uses'=> 'AttributionsController@altaa',
      'as'  => 'administracion.atribuciones.alta'
  ]);
  
 Route::get('atribuciones/bajas/index', [
      'uses'=> 'AttributionsController@bajasaindex',
      'as'  => 'atribuciones.baja.index'
  ]);



  //*******************************************
  //*******************************************


//OTROS: ASIGNACION DE GRADOS A ALUMNOS

  Route::resource('otros', 'OtrosController');
  Route::get('otros/destroy/{id}', [
      'uses'=> 'OtrosController@destroy',
      'as'  => 'administracion.otros.destroy'
  ]);


 Route::get('otros/alta/{id}', [
      'uses'=> 'OtrosController@altao',
      'as'  => 'administracion.otros.alta'
  ]);
  
 Route::get('otros/bajas/index', [
      'uses'=> 'OtrosController@bajasoindex',
      'as'  => 'otros.baja.index'
  ]);



 //**************************************************
 //**************************************************
  



  Route::resource('indicadors', 'IndicadorController');
  Route::get('indicadors/destroy/{id}', [
      'uses'=> 'IndicadorController@destroy',
      'as'  => 'administracion.indicadors.destroy'
  ]);

  Route::resource('avance_indicadors', 'AvanceIndicadorController');
  Route::get('avance_indicadors/destroy/{id}', [
      'uses'=> 'AvanceIndicadorController@destroy',
      'as'  => 'administracion.avance_indicadors.destroy'
  ]);

 /* Route::resource('lista_baja', 'BareaController');
  Route::get('lista_baja/destroy/{id}', [
      'uses'=> 'BareaController@destroy',
      'as'  => 'administracion.lista_baja.destroy'
  ]);*/
  Route::resource('indicador_baja', 'BIndicadors');
  Route::get('indicador_baja/destroy/{id}', [
      'uses'=> 'BIndicadors@destroy',
      'as'  => 'administracion.indicador_baja.destroy'
  ]);


Route::resource('notas', 'NotasController');
  Route::get('notas/destroy/{id}', [
      'uses'=> 'NotasController@destroy',
      'as'  => 'administracion.notas.destroy'
  ]);
//notas
  Route::get('lenguaje/{idasignatura}');
  Route::get('notas/edit_matematica/{idgrado}/{idasignatura}/{id}', [
              'uses'=> 'NotasController@edit_matematica',
              'as'  => 'administracion/notas/edit_matematica'
  ]);

Route::get('notas/edit_mat/{idgrado}',[
            'uses'=> 'NotasController@edit_mat',
            'as'  => 'administracion.notas.edit_mat'
]);

Route::get('notas/edit_historial/{idgrado}/{idalumno}', [
            'uses'=> 'NotasController@edit_historial',
            'as'  => 'administracion.notas.edit_historial'
]);
Route::get('notas/edit_final/{idgrado}/{idalumno}', [
            'uses'=> 'NotasController@edit_final',
            'as'  => 'administracion.notas.edit_final'
]);
Route::get('notas/edit_finales/{idgrado}/{idalumno}', [
            'uses'=> 'NotasController@edit_finales',
            'as'  => 'administracion.notas.edit_finales'
]);

Route::get('notas/edit_trimestre/{idgrado}/{idasignatura}/{id}', [
            'uses'=> 'NotasController@edit_trimestre',
            'as'  => 'administracion/notas/edit_trimestre'
]);

Route::get('notas/edit_trimestres/{idgrado}/{idasignatura}/{id}', [
            'uses'=> 'NotasController@edit_trimestres',
            'as'  => 'administracion/notas/edit_trimestres'
]);
//conductas
Route::resource('conductas', 'ConductasController');
Route::get('conductas/editar/{idgrado}', [
            'uses'=> 'ConductasController@editar',
            'as'  => 'administracion/conductas/editar'
]);
Route::get('conductas/edit_conducta/{idgrado}', [
            'uses'=> 'ConductasController@edit_conducta',
            'as'  => 'administracion/conductas/edit_conducta'
]);
Route::get('conductas/edit_conductas/{idgrado}', [
            'uses'=> 'ConductasController@edit_conductas',
            'as'  => 'administracion/conductas/edit_conductas'
]);


//registro de indicadores
Route::resource('indicador_assignment', 'IndAssingController');
Route::get('indicador_assignment/destroy/{id}', [
      'uses'=> 'IndAssingController@destroy',
      'as'  => 'administracion.indicador_assignment.destroy'
  ]);
Route::get('multiple-inserting',[
      'uses'=> 'IndAssingController@multipleInsert',
      'as'  => 'administracion.indicador_assignment.multiple-insert'
  ]);
Route::get('cargar-formulario',[
      'uses'=> 'IndAssingController@cargarFormulario',
      'as'  => 'administracion.indicador_assignment.cargar-formulario'
  ]);
Route::get('area/{area}/{doc}/{app}/{nivel}',[
      'uses'=> 'IndAssingController@registrar',
      'as'  => 'indicador_assignment.registrar'
  ]);
Route::get('cargar-registros',[
      'uses'=> 'IndAssingController@cargarRegistros',
      'as'  => 'administracion.indicador_assignment.cargar-registros'
  ]);


});

//MANEJO DE ERROR BASE
Route::get('error', function(){ 
    abort(500);
});

Route::get('usuarios/password', 'PasswordController@password');
Route::post('usuarios/updatepassword', 'PasswordController@updatePassword');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
