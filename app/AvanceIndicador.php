<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvanceIndicador extends Model
{
    protected $fillable = [
        'id_alumno', 'id_indicador', 'id_anio', 'trimestre', 'avance','activo'
    ];



    public function indicador(){
        return $this->belongsTo('App\Indicador', 'id_indicador');
    }

    public function anio_escolar(){
        return $this->belongsTo('App\AnioEscolar', 'id_anio');
    }

    public function alumno(){
        return $this->belongsTo('App\Alumno', 'id_alumno');
    }

}
