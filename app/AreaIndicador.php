<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaIndicador extends Model
{
     protected $fillable = [
      'nombre','timestamps', 'activo'
  ];

     public function indicadores(){
        return $this->hasMany('App\Indicador');
    }
}
