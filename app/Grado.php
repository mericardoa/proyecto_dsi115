<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $fillable =['idturno', 'nombre'];

    public function turnos(){
    	return $this->belongsTo('App\Turno', 'idturno');
    }

    public function relaciones(){
        return $this->hasMany('App\Relationship');
    }

     public function alumnos(){
        return $this->hasMany('App\Alumno');
    }

    //public function docentes_grados(){
    //    return $this->belongsTo('App\Docente_Grado', 'idgrado');
    //}
}
