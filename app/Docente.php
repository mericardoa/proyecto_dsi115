<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    //public $table = "docentes";
    protected $fillable = [
        'nip', 'nombres', 'apellidos', 'dui', 'nit', 'especialidad'
    ];

    public function asignaturas(){
        return $this->belongsToMany('App\Asignatura');
    }

    public function users(){
        return $this->hasMany('App\User');
    }

    public function assignments(){
        return $this->hasMany('App\Assignment');
    }

    //public function docentes_grados(){
    //    return $this->belongsTo('App\Docente_Grado', 'iddocente');
    //}

}
