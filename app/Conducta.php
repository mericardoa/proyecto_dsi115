<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conducta extends Model
{
  protected $fillable =['idrecord', 'c1', 'c2', 'c3', 'c4', 'c5', 'idtrimestre'];

  public function notas(){
      return $this->hasMany('App\Nota');
}

public function records(){
    return $this->belongsTo('App\Record', 'idrecord');
}
public function trimestres(){
    return $this->belongsTo('App\Trimestre', 'idtrimestre');
}
}
