<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Grado;
use Illuminate\Http\Request;
use App\Http\Requests\AlumnoRequest;
use Validator;

class AlumnosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
     {
       $alumnos= Alumno::orderBy('id', 'ASC')->paginate(250);
       $carbon = new \Carbon\Carbon();
                        $date25 = $carbon->now();
                        $year = $date25->format('Y');
                   

       return view('administracion.alumnos.index')->with('alumnos', $alumnos)->with('year', $year);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
         $grados = Grado::orderBy('id', 'ASC')->pluck('nombre', 'id');

         return view('administracion.alumnos.create')->with('grados', $grados);
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(AlumnoRequest $request)
     {
         $alumno=new Alumno($request->all());
         $alumno->save();
         flash()->success('Se ha registrado el alumno de forma exitosa');

         return redirect()->route('alumnos.index');


     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         $alumno=Alumno::find($id);
         return view('administracion.alumnos.edit')->with('alumno', $alumno);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(AlumnoRequest $request, $id)
     {
         $alumno=Alumno::find($id);
         $alumno->fill($request->all());
         $alumno->save();

         flash()->success('Los datos han sido actualizado con exito');

         return redirect()->route('alumnos.index');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
        $alumno=Alumno::find($id);
         $alumno->activo=0;
         $alumno->save();

         flash()->success('El alumno '.$alumno->nombres.' ha sido dado de baja');
         return redirect()->route('alumnos.index');        
     }

     public function alta($id)
     {
        $alumno=Alumno::find($id);
         $alumno->activo=1;
         $alumno->save();

         flash()->success('El alumno '.$alumno->nombres.' ha sido dado de alta');
         return redirect()->route('alumnos.index');        
     }

     public function bajasindex()
    {
          $alumnos= Alumno::orderBy('id', 'ASC')->paginate(10);
       return view('administracion.alumnos.bajas')->with('alumnos', $alumnos);
    }
}
