<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Record;
use App\Alumno;
use App\Assignment;
use App\Grado;
use App\Http\Requests\RecordRequest;
use DB;

class RecordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      //dd($request->get('idassignment'));

      //dd($request->get('identificador'));
    //$registros = Record::find($request);
    //int $identificador= Int32.Parse($registros);

    $registros = Assignment::with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(250);

    return view('administracion.registros.index')->with('registros', $registros)->with('registros', $registros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $alumnos = Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
      $identificador = Grado::orderBy('id', 'ASC')->pluck('nombre', 'id');
      //$asig = Grado::orderBy('id', 'ASC')->pluck('nombre','id');
      //$asig=$asig2->id;
      //$asig->grados()->nombre;

      return view('administracion.registros.create')->with('alumnos', $alumnos)->with('identificador', $identificador);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecordRequest $request)
    {
      $record=new Record($request->all());
      $record->save();
      flash()->success('Se ha registrado la asignacion de forma exitosa');

      return redirect()->route('registros.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $identificador = Record::find($id);

      //$registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
      $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
      $grados = Grado::orderBy('id', 'ASC')->pluck('nombre','id');

      return view('administracion.registros.edit')->with('identificador', $identificador)->with('grados', $grados)->with('alumnos', $alumnos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /*$identificador=Grado::find($id);

        $registros=new Record($request->all());
        $registros->fill($request->all());
        $registros->save();

        flash()->warning('La asignacion ha sido almacenada con exito');

        return redirect()->route('registros.edit', $identificador);*/
        $registros=Record::find($id);
        $registros->fill($request->all());
        $registros->save();

        flash()->success('Los datos del registro han sido modificados');

        return redirect()->route('otros.edit', $registros->idgrado);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
