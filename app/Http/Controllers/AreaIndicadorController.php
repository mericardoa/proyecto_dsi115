<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AreaIndicador;

class AreaIndicadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area_indicadores= AreaIndicador::orderBy('id', 'ASC')->paginate(250);
      return view('administracion.areaindicadores.index')->with('area_indicadores', $area_indicadores);
    }

/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administracion.areaindicadores.create');
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area_indicadores=new AreaIndicador($request->all());
        $area_indicadores->save();
        flash()->success('Se ha registrado el area del indicador correctamente');

        return redirect()->route('area_indicadores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $area_indicadores=AreaIndicador::find($id);
        return view('administracion.areaindicadores.baja')->with('area_indicadores', $area_indicadores);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area_indicadores=AreaIndicador::find($id);
        return view('administracion.areaindicadores.edit')->with('area_indicadores', $area_indicadores);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $area_indicadores=AreaIndicador::find($id);
        $area_indicadores->fill($request->all());
        //$area_indicadores->save();


        if ($area_indicadores->activo == 0) {
            $area_indicadores->save();
     flash()->success('El Registro del Area '. $area_indicadores->nombre. ' ha sido dado de baja');

        return redirect()->route('area_indicadores.index');
    }

 else {
    $area_indicadores->save();
        flash()->success('Los datos han sido actualizados con exito');

        return redirect()->route('area_indicadores.index');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area_indicador = AreaIndicador::find($id);
        $area_indicador->activo=0;
        $area_indicador->save();
        flash()->success('La Area '.$area_indicador->nombre.' ha sido dado de baja');
        return redirect()->route('area_indicadores.index');
    }

    public function bajasaiindex()
    {
         $area_indicadores= AreaIndicador::orderBy('id', 'ASC')->paginate(250);
         return view('administracion.areaindicadores.baja')->with('area_indicadores', $area_indicadores);
    }

    public function altaai($id)
    {
        $area_indicador = AreaIndicador::find($id);
        $area_indicador->activo=1;
        $area_indicador->save();
        flash()->success('La Area '.$area_indicador->nombre.' ha sido dado de alta');
        return redirect()->route('area_indicadores.index');
          
    }
}
