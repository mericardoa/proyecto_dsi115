<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribution;
use App\Grado;
use App\Asignatura;
use App\Http\Requests\AttributionRequest;

class AttributionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $atribuciones = Attribution::with('grados', 'asignaturas')->orderBy('id', 'ASC')->paginate(250);

      return view('administracion.atribuciones.index')->with('atribuciones', $atribuciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $grados = Grado::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $asig = Asignatura::orderBy('id', 'ASC')->pluck('nombre', 'id');

      return view('administracion.atribuciones.create')->with('grados', $grados)->with('asig', $asig);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributionRequest $request)
    {
      $attribution = new Attribution($request->all());
      $attribution->save();
      flash()->success('Se ha registrado la asignacion de forma exitosa');

      return redirect()->route('atribuciones.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $atribuciones=Attribution::find($id);

      $asignaturas = Asignatura::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $grados = Grado::orderBy('id', 'ASC')->pluck('nombre','id');

      return view('administracion.atribuciones.edit')->with('asignaturas', $asignaturas)->with('grados', $grados)->with('atribuciones', $atribuciones);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AttributionRequest $request, $id)
    {
      $atribuciones=Attribution::find($id);
      $atribuciones->fill($request->all());
      $atribuciones->save();

      flash()->success('Los datos de la asignacion han sido modificados');

      return redirect()->route('atribuciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $atribuciones = Attribution::find($id);
        $atribuciones->activo=0;
        $atribuciones->save();
        flash()->success('La asignacion '.$atribuciones->grados->nombre.' ha sido dada de baja');
        return redirect()->route('atribuciones.index');
    }

    public function bajasaindex()
    {
        $atribuciones= Attribution::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.atribuciones.bajas')->with('atribuciones', $atribuciones);
    }

    public function altaa($id)
    {
        $atribucion = Attribution::find($id);
        $atribucion->activo=1;
        $atribucion->save();
        flash()->success('La asignacion '.$atribucion->grados->nombre.' ha sido dada de alta');
        return redirect()->route('atribuciones.index');
          
    }
}
