<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Asignatura;
use App\Record;
use App\Grado;
use App\Attribution;
use Session;
use Auth;
use App\Assignment;
use App\Nota;
use App\Trimestre;
use App\Conducta;
use App\Http\Requests\NotaRequest;
//use Illuminate\Foundation\Validation\ValidatesRequests;
use ValidatesRequests;

use Illuminate\Support\Facades\Route;

class NotasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $alumnos=Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
      $registros = Record::orderBy('id', 'ASC')->pluck('idalumno', 'id');
       $trimestres =Trimestre::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $asig = Asignatura::orderBy('id', 'ASC')->pluck('nombre', 'id');

      return view('administracion.notas.create')->with('registros', $registros)->with('alumnos', $alumnos)->with('asig', $asig)->with('trimestres', $trimestres);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      //for ($i=0; $i < 4; $i++) {
          $asignatura=new Nota($request->all());
          $asignatura->promedio = round(($asignatura->nota1+$asignatura->nota2+$asignatura->nota3)/3);
          $asignatura->save();


    //  }
      //$asignatura=new Nota($request->all());
      //$asignatura->save();
      $nombre = $asignatura->records->alumnos->nombres;
      $apellido = $asignatura->records->alumnos->apellido_padre;
      $apellidom = $asignatura->records->alumnos->apellido_madre;
      flash()->success('Las Notas del Alumn@ '.$nombre.' '.$apellido.' '.$apellidom.' han sido registradas de forma exitosa');

      $var = $asignatura->records->grados->id;
      $va = $asignatura->asignaturas->id;
      $id = $asignatura->idrecord;

      if($asignatura->idtrimestre == 1)
      {
        return redirect()->route('administracion/notas/edit_matematica', ['idgrado' => $var, 'idasignatura' => $va, 'id' => $id]);
      }elseif ($asignatura->idtrimestre == 2) {
        return redirect()->route('administracion/notas/edit_trimestre', ['idgrado' => $var, 'idasignatura' => $va, 'id' => $id]);
      }else {
        return redirect()->route('administracion/notas/edit_trimestres', ['idgrado' => $var, 'idasignatura' => $va, 'id' => $id]);
      }

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($idgrado)
     {
       $identificador = Grado::find($idgrado);
      // $asignatura = Asignatura::find($idasignatura);

     //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $trimestres =Trimestre::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');

       return view('administracion.notas.edit')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos)->with('asignaturas', $asignaturas)->with('trimestres', $trimestres);
     }
    //Primer Trimestre
     public function edit_matematica($idgrado, $idasignatura, $id)
     {
       $identificador = Grado::find($idgrado);
       $asig = Asignatura::find($idasignatura);
       $i = $id;

     //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
      //$conductas = Conducta::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
       $notas = Nota::orderBy('id', 'ASC');

       return view('administracion.notas.edit_matematica')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos)->with('notas', $notas)->with('asig', $asig)->with('i', $i);
     }

     //Segundo Trimestre
     public function edit_trimestre($idgrado, $idasignatura, $id)
     {
       $identificador = Grado::find($idgrado);
       $asig = Asignatura::find($idasignatura);
       $i = $id;

     //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
       //$conductas = Conducta::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
       $notas = Nota::orderBy('id', 'ASC');

       return view('administracion.notas.edit_trimestre')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos)->with('notas', $notas)->with('asig', $asig)->with('i', $i);
     }

     //Tercer Trimestre
     public function edit_trimestres($idgrado, $idasignatura, $id)
     {
       $identificador = Grado::find($idgrado);
       $asig = Asignatura::find($idasignatura);
       $i = $id;

     //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
      //$conductas = Conducta::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
       $notas = Nota::orderBy('id', 'ASC');

       return view('administracion.notas.edit_trimestres')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos)->with('notas', $notas)->with('asig', $asig)->with('i', $i);
     }




     public function edit_historial($idgrado, $idalumno)
     {
       $identificador = Grado::find($idgrado);
       $alumno = Alumno::find($idalumno);
       $asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $atribuciones = Attribution :: with('grados', 'asignaturas')->orderBy('id', 'ASC')->paginate(20);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
       $notas = Nota::orderBy('id', 'ASC')->paginate(40);
       $conductas = Conducta::orderBy('id', 'ASC')->paginate(40);




       return view('administracion.notas.edit_historial')->with('identificador', $identificador)->with('alumno', $alumno)->with('notas', $notas)->with('asignaturas', $asignaturas)->with('atribuciones', $atribuciones)->with('conductas', $conductas);
     }

     public function edit_final($idgrado, $idalumno)
     {
       $identificador = Grado::find($idgrado);
       $alumno = Alumno::find($idalumno);
       $asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
       $atribuciones = Attribution :: with('grados', 'asignaturas')->orderBy('id', 'ASC')->paginate(20);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
       $notas = Nota::orderBy('id', 'ASC')->paginate(40);
       $conductas = Conducta::orderBy('id', 'ASC')->paginate(40);
       $suma = 0;

       if($idgrado > '12')
       {
         return view('administracion.notas.edit_finales')->with('identificador', $identificador)->with('alumno', $alumno)->with('notas', $notas)->with('asignaturas', $asignaturas)->with('atribuciones', $atribuciones)->with('conductas', $conductas)->with('suma', $suma);
       }else {
         return view('administracion.notas.edit_final')->with('identificador', $identificador)->with('alumno', $alumno)->with('notas', $notas)->with('asignaturas', $asignaturas)->with('atribuciones', $atribuciones)->with('conductas', $conductas)->with('suma', $suma);
       }
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $identificador=Grado::find($id);
    //  $ident=Alumno::find($id);
      $atribuciones = Attribution ::with('grados', 'asignaturas')->orderBy('id', 'ASC');
      $notas=new Nota($request->all());
      $notas->fill($request->all());
    //  $registros->$idalumno->asignado=1;
      $notas->save();

      flash()->warning('La asignacion ha sido almacenada con exito');

      return redirect()->route('administracion.notas.edit_matematica', $identificador);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}                                                                                                                                                                                                             
