<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Docente;

class BDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docentes= Docente::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.docentes.bajas')->with('docentes', $docentes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function baja($id)
    {
        $docente=Docente::find($id);
        //$docente->fill($request->all());
        $docente->activo=0;
        $docente->save();

        flash()->warning('El registro ha sido dado de baja');

        return redirect()->route('bajadoc.index');
    }

    public function bajas()
    {
        $docentes= Docente::orderBy('id', 'ASC')->paginate(10);

        return view('administracion.docentes.bajas')->with('docentes', $docentes);
    }
    
}
