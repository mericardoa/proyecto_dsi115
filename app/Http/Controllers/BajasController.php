<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Docente;
use App\Assignment;
use App\Record;
use App\Attribution;

class BajasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $alumnos= Alumno::orderBy('id', 'ASC')->paginate(250);
       return view('administracion.bajas.index')->with('alumnos', $alumnos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function baja($id)
    {
        $alumno=Alumno::find($id);
         $alumno->activo=0;
         //fill($request->all());
         $alumno->save();

         flash()->warning('El registro ha sido dado de baja');

         return redirect()->route('baja.index');
    }

    public function bajasalumnos()
    {
          $alumnos= Alumno::orderBy('id', 'ASC')->paginate(250);
       return view('administracion.alumnos.bajas')->with('alumnos', $alumnos);
    }

    public function bajasdocentes()
    {
        $docentes= Docente::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.docentes.bajas')->with('docentes', $docentes);
    }


    //BAJA GRADO
    public function bajasgrados()
    {
        $grados= Grado::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.grados.baja')->with('grados', $grados);
    }

//******************************
    //BAJA areaINDICADORES

    public function bajasarea_indicadores()
    {
        $area_indicadores= AreaIndicador::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.areaindicadores.baja')->with('area_indicadores', $area_indicadores);
    }


    public function bajasindicadores()
    {
        $indicadores= Indicador::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.areaindicadores.baja')->with('indicadores', $indicadores);
    }




//*******************************

    public function bajasassignments()
    {
        $asignaciones= Assignment::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.asignaciones.bajas')->with('asignaciones', $asignaciones);
    }
    public function altasassignments()
    {
        $asignaciones= Assignment::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.asignaciones.bajas')->with('asignaciones', $asignaciones);
    }

    public function bajasrecords()
    {
        $registros= Record::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.registros.bajas')->with('registros', $registros);
    }

    public function bajasattributions()
    {
        $atribuciones= Attribution::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.atribuciones.bajas')->with('atribuciones', $atribuciones);
    }


}
