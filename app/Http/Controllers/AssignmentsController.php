<?php

namespace App\Http\Controllers;
use App\Assignment;
use App\Docente;
use App\Grado;
use App\Asignatura;
use App\Http\Requests\AssignmentRequest;
use Illuminate\Http\Request;

class AssignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // $asigna=Assignment::orderBy('id', 'ASC')->paginate(10);
      //$docentes=Docente::orderBy('id', 'ASC')->paginate(10);
    //  return view('administracion.asignaciones.index')->with('asigna', $asigna);

      $asigna = Assignment::with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(250);
    //  $asigna = Assignment::search($request->idgrado)->orderBy('id', 'ASC')->paginate(10);

      return view('administracion.asignaciones.index')->with('asigna', $asigna);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $docentes = Docente::orderBy('id', 'ASC')->pluck('nombres', 'id');
      $grados = Grado::orderBy('id', 'ASC')->pluck('nombre','id');
      //$asignaturas = Asignatura::orderBy('id', 'ASC')->pluck('nombre','id');

      return view('administracion.asignaciones.create')->with('docentes', $docentes)->with('grados', $grados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignmentRequest $request)
    {
      $assignment=new Assignment($request->all());
      $assignment->save();
      flash()->success('Se ha registrado la asignacion de forma exitosa');

      return redirect()->route('asignaciones.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asignaciones=Assignment::find($id);

        $docentes = Docente::orderBy('id', 'ASC')->pluck('nombres', 'id');
        $grados = Grado::orderBy('id', 'ASC')->pluck('nombre','id');

        return view('administracion.asignaciones.edit')->with('docentes', $docentes)->with('grados', $grados)->with('asignaciones', $asignaciones);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asignaciones=Assignment::find($id);
        $asignaciones->fill($request->all());
        $asignaciones->save();

        flash()->success('Los datos de la asignacion han sido modificados');

        return redirect()->route('asignaciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      /*$asignaciones = Assignment::find($id);
      $asignaciones->delete();

        flash()->error('La asignacion ha sido efectudada de manera exitosa');
        return redirect()->route('asignaciones.index');*/
        $asignaciones = Assignment::find($id);
        $asignaciones->activo=0;
        $asignaciones->save();

        flash()->success('La asignacion '.$asignaciones->docentes->nombres.' ha sido dado de baja');

        return redirect()->route('asignaciones.index');

    }

     public function alta($id)
    {
        $assignment = Assignment::find($id);
        $assignment->activo=1;
        $assignment->save();
        flash()->success('La asignacion '.$assignment->docentes->nombres.' ha sido dado de alta');
        return redirect()->route('asignaciones.index');
          
    }
}
