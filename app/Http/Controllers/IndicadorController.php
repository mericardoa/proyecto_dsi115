<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Indicador;
use App\AreaIndicador;

class IndicadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indicadors = Indicador::with('area_indicador')->orderBy('id', 'ASC')->paginate(10);

        return view('administracion.indicadores.index')->with('indicadors', $indicadors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$area_indicadors = AreaIndicador::orderBy('id', 'ASC')->pluck('nombre', 'id');
        $area_indicadors = AreaIndicador::where("activo","=",1)->orderBy('id', 'ASC')->pluck('nombre', 'id');

        //$users = User::where("estado","=",1)->select("id","username")->paginate(10);


        return view('administracion.indicadores.create')->with('area_indicadors', $area_indicadors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $indicadors = new Indicador($request->all());
        $indicadors->save();

        flash()->success('Se ha registrado el indicador de forma exitosa');

        return redirect()->route('indicadors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $indicadors = Indicador::find($id);
        $indicadors->area_indicador;

        $area_indicadors = AreaIndicador::orderBy('id', 'ASC')->pluck('nombre', 'id');
        return view('administracion.indicadores.baja')
            ->with('area_indicadors', $area_indicadors)
            ->with('indicadors', $indicadors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $indicadores = Indicador::find($id);
        $indicadores->area_indicador;

        $area_indicadores = AreaIndicador::orderBy('id', 'ASC')->pluck('nombre', 'id');
        return view('administracion.indicadores.edit')
            ->with('area_indicadores', $area_indicadores)
            ->with('indicadores', $indicadores);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $indicadors = Indicador::find($id);
        $indicadors->fill($request->all());

         if ($indicadors->activo == 0) {
            $indicadors->save();
     flash()->success('El Registro del Area '. $indicadors->nombre. ' ha sido dado de baja');

        return redirect()->route('indicadors.index');
    }

 else {
    $indicadors->save();
        flash()->success('Los datos han sido actualizados con exito');

        return redirect()->route('indicadors.index');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $indicador = Indicador::find($id);
        $indicador->activo=0;
        $indicador->save();
        flash()->success('El indicador '.$indicador->nombre.' ha sido dado de baja');
        return redirect()->route('indicadores.index');
    }

    public function bajasiindex()
    {
         $indicadores= Indicador::orderBy('id', 'ASC')->paginate(250);
         return view('administracion.indicadores.baja')->with('indicadores', $indicadores);
    }

    public function altai($id)
    {
        $indicador = Indicador::find($id);
        $indicador->activo=1;
        $indicador->save();
        flash()->success('El indicador '.$indicador->nombre.' ha sido dado de alta');
        return redirect()->route('indicadores.index');
          
    }
}
