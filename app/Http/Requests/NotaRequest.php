<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;

class NotaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        'idrecord'=>'required:notas',
        'idtrimestre'=>'required:notas',
        'nota1'                 => 'required|numeric:notas|between:0,10',
        'nota2'                 => 'required|numeric:notas|between:0,10',
        'nota3'                 => 'required|numeric:notas|between:0,10',
        'idasignatura'=>'required:notas',


        ];
    }


    public function messages(){
        return [
          /*  'nie.required' => 'El campo NIE es requerido',
            'nie.numeric' => 'El campo NIE solo acepta numeros',
            'nie.min' => 'El mínimo permitido para NIE son 6 caracteres',
            'nie.max' => 'El máximo permitido para NIE son 8 caracteres',

            'nombres.required' => 'El campoNombre es requerido',
            'nombres.min' => 'El mínimo permitido para Nombres son 6 caracteres',
            'nombres.max' => 'El maximo permitido para Nombres son 6 caracteres',
            'nie.regex' => 'El campo Nombres solo acepta letras',

            'responsable.required' => 'El campo Responsable es requerido',
            'responsable.min' => 'El mínimo permitido para Responsable son 6 caracteres',
            'responsable.max' => 'El maximo permitido para Responsable son 6 caracteres',
            'responsable.regex' => 'El campo Responsable solo acepta letras',
*/


        ];
    }
}
