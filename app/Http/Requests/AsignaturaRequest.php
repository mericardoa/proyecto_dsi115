<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AsignaturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'nombre'   =>'required|min:3|max:20|regex:/^[a-z\sáéíóú]+$/i',
        ];
    }

    public function messages(){
        return [
            'nombre.required' => 'El campo Nombre es requerido',
            'nombre.min' => 'El mínimo permitido para Nombres son 4 caracteres',
            'nombre.max' => 'El maximo permitido para Nombres son 20 caracteres',
            'nombre.regex' => 'El campo Nombre solo acepta letras',
        ];
    }



}
