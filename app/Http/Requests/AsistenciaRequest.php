<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class AsistenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      

        return [
          
            'estado'=>'required',
            //'idrecord'=>'required',
           'fecha' => 'required',


            
        ];
    }

    public function messages(){
        return [
            'fecha.unique' => 'Esta fecha ya fue registrada',
    
        ];
    }




}