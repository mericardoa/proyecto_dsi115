<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocenteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nip'       =>'min:7|max:7|required|unique:docentes',
            'nombres'   =>'min:3|max:100|required|regex:/^[a-z\sáéíóú]+$/i',
            'apellidos' =>'min:4|max:100|required|regex:/^[a-z\sáéíóú]+$/i',
            'dui'       =>'min:10|max:10|required|unique:docentes',
            'nit'       =>'min:17|max:17|required|unique:docentes',
            'especialidad'  =>'min:5|Required|regex:/^[a-z\sáéíóú]+$/i'
        ];
        }

        public function messages(){
            return [
                'nip.required' => 'El campo NIP es requerido',
                'nip.min' => 'El mínimo permitido para NIP son 7 caracteres',
                'nip.max' => 'El maximo permitido para NIP son 7 caracteres',
                'nip.unique' => 'El campo NIP debe ser unico',

                'nombres.required' => 'El campo Nombres es requerido',
                'nombres.min' => 'El mínimo permitido para Nombres son 3 caracteres',
                'nombres.max' => 'El maximo permitido para Nombres son 100 caracteres',
                'nombres.regex' => 'El campo Nombres solo acepta letras',

                'apellidos.required' => 'El campo Nombres es requerido',
                'apellidos.min' => 'El mínimo permitido para Apellidos son 4 caracteres',
                'apellidos.max' => 'El maximo permitido para Apellidos son 100 caracteres',
                'apellidos.regex' => 'El campo Apellidos solo acepta letras',

                'dui.required' => 'El campo DUI es requerido',
                'dui.max' => 'El maximo permitido para DUI son 8 caracteres',
                'dui.min' => 'El mínimo permitido para DUI son 8 caracteres',
                'dui.unique' => 'El campo DUI debe ser unico',
//CAMBIOS
                'nit.required' => 'El campo NIT es requerido',
                'nit.min' => 'El mínimo permitido para NIT son 17 caracteres',
                'nit.max' => 'El maximo permitido para NIT son 17 caracteres',
                'nit.unique' => 'El campo NIT debe ser unico',

                'especialidad.required' => 'El campo Especialidad es requerido',
                'especialidad.min' => 'El mínimo permitido para Especialidad son 3 caracteres',
                'especialidad.regex' => 'El campo Especialidad solo acepta letras',

            ];
        }
}
