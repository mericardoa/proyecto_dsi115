<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribution extends Model
{
    protected $fillable=['idgrado', 'idasignatura'];

    public function grados(){
        return $this->belongsTo('App\Grado', 'idgrado');
    }

    public function asignaturas(){
        return $this->belongsTo('App\Asignatura', 'idasignatura');
    }
}
