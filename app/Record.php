<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $fillable=['idalumno', 'idgrado'];

    public function alumnos(){
        return $this->belongsTo('App\Alumno', 'idalumno');
    }

    public function grados(){
        return $this->belongsTo('App\Grado', 'idgrado');
    }

    public function scopeSearch($query, $idgrado)
    {
      switch ($idgrado) {
        case "Primero A":
          $idgrado="1";
          break;
        case "Primero B":
          $idgrado="2";
          break;
        case "Segundo A":
          $idgrado="3";
          break;
        case "Segundo B":
          $idgrado="4";
          break;
        case "Tercero A":
          $idgrado="5";
          break;
        case "Tercero B":
          $idgrado="6";
          break;
        case "Cuarto A":
          $idgrado="7";
          break;
        case "Cuarto B":
          $idgrado="8";
          break;
        case "Quinto A":
          $idgrado="9";
          break;
        case "Quinto B":
          $idgrado="10";
          break;
        case "Sexto A":
          $idgrado="11";
          break;
        case "Sexto B":
          $idgrado="12";
           break;
        case "Septimo A":
          $idgrado="13";
          break;
        case "Septimo B":
          $idgrado="14";
           break;
        case "Octavo A":
          $idgrado="15";
          break;
        case "Octavo B":
          $idgrado="16";
          break;
        case "Noveno A":
          $idgrado="17";
          break;
        case "Noveno B":
          $idgrado="18";
          break;
        default:
          # code...
          break;
      }
    /*  if($idgrado=="Primero A")
      {
        $idgrado="1";
      }elseif ($idgrado=="Primero B") {
        $idgrado="2";
      }*/

      return $query->where('idgrado', $idgrado);
    }

     public function areas1(){
        return $this->hasMany('App\Area1');
    }

    public function areas2(){
        return $this->hasMany('App\Area2');
    }
    public function areas3(){
        return $this->hasMany('App\Area3');
    }


}
